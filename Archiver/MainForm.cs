﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Archiver
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			InitDataFolder();
		}

		private bool InitDataFolder()
		{
			if (Properties.Settings.Default.DataFolderPath == "")
			{
				if (dataFolderDialog.ShowDialog() == DialogResult.OK)
				{
					Properties.Settings.Default.DataFolderPath = dataFolderDialog.SelectedPath;
				}
				else
				{
					return false;
				}

				Properties.Settings.Default.DataFolderPath = dataFolderDialog.SelectedPath;
				Properties.Settings.Default.Save();

				return true;
			}

			if (Directory.Exists(Properties.Settings.Default.DataFolderPath)) return true;

			Properties.Settings.Default.DataFolderPath = "";
			Properties.Settings.Default.Save();
			return false;
		}
	}
}
